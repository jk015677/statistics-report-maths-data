## Introduction

Relationships of student characteristics to student performance and attitudes were conducted within two secondary schools in Portugal. 132 students characteristics (Age, Gender, Study Time, Paid Class, Activities Participation, Health, Absence Had, Internet connection, and the parents' education) were selected for inclusion in the investigation and are included in the dataset. If you have seen the dataset provided, the math grades are from 3 tests split up over the course of a year. These grades are labelled G1, G2, and G3, where each test has a limit of 20 marks. With the use of the dataset, this study aims to create a link between the student characteristics and see if they affected the students' math grades. It is hoped that this study will inform students in Portugal what the best characteristic is for gaining better Maths grades. 

This report's format will be straightforward: Method, where we will be discussing which statistical method will be deemed the most effective in drawing a comparison between a choice characteristic and maths grade. After that, with the SAS results, we will be discussing the results and how they differ from the hypothesis. Finally, we will be concluding to summarize or reflect on our findings. We will enlist our code in the end under the appendix. We also develop a presentation to answer certain questions that Our supervisors will ask about the dataset. 


The selected hypothesis we are going to test will be:

1. Correlation between absence and grades
2. Paid Class will improve math grades drastically
3. The 1st school has a better average than the 2nd school

## Our First Overview Of the Provided DataSet

This section will discuss what our first initiation thought was when looking into the dataset. This will give insight into why we have chosen the selected hypothesis and why we were interested. 

Within this secondary school, there are 132 students in these Portugal schools with ages between 19 and 15. However, 2 students are 20 and 1 that is 21. Believing that this was an outlier, we googled students' acceptance over 18, and it states that secondary has to be between 15 -18[1]. However, since the 2nd secondary class has accepted both 20-year-olds, I can see that they will be lenient to a 21-year-old student. We can say fairly that the outliers are not false information and should not be ignored. Furthermore, we looked in detail at the different variables that have been obtained, and we are trying to find the link between the personality traits and their performance in exams. The characteristic we were first interested in was absence. We will be using a linear regression to check if theirs a correlation between absences and grades. The second characteristic we will be looking into will be paid class. To see if additional classes will lead to getting better maths grades will be interesting if proven wrong. We also want to draw a comparison to both given schools using a box plot. To see what school performs better or has a better average in Mathematics grades. .



## References

[1] Key features of the education system https://eacea.ec.europa.eu/national-policies/eurydice/content/portugal_en