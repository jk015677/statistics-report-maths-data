# Statistics Report - Maths Data


Module Code: ST1PS

Assignment report Title:  Stats Report Group Project

Student Number : 28012937, 29015642, 29015677, 29021376, 28013149

Date (when the work completed): 19 March 2021

Actual hrs spent for the assignment: 17 hours 

Assignment evaluation (3 key points): 1. Matching up methods on the data was challenging as we have to pick several methods to match the given hypothesis.
                                      2. Learnt how to program in SAS successfully, now having knowledge on creating a t-test.
                                      3. Page limits were annoying to deal with as we had to cut down work. 






